$(document).ready(function() {
  svg4everybody();

  $('#s-photo-gallery-slider').owlCarousel({
    dots: false,
    nav: true,
    smartSpeed: 500,
    navText: [
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 31.49"><path d="M21.205 5.007c-.43-.444-1.143-.444-1.587 0-.43.43-.43 1.143 0 1.57l8.047 8.048H1.11c-.618 0-1.11.493-1.11 1.112 0 .62.492 1.127 1.11 1.127h26.555l-8.047 8.032c-.43.444-.43 1.16 0 1.587.444.444 1.16.444 1.587 0l9.952-9.952c.444-.428.444-1.142 0-1.57l-9.952-9.953z"/></svg>',
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 31.49"><path d="M21.205 5.007c-.43-.444-1.143-.444-1.587 0-.43.43-.43 1.143 0 1.57l8.047 8.048H1.11c-.618 0-1.11.493-1.11 1.112 0 .62.492 1.127 1.11 1.127h26.555l-8.047 8.032c-.43.444-.43 1.16 0 1.587.444.444 1.16.444 1.587 0l9.952-9.952c.444-.428.444-1.142 0-1.57l-9.952-9.953z"/></svg>'],
    responsive: {
      0: {
        items: 1,
        mouseDrag: true,
        margin: 15,
      },
      480: {
        items: 2,
        mouseDrag: true,
        margin: 15,
      },
      768: {
        items: 3,
        mouseDrag: true,
        margin: 30,
      },
      992: {
        items: 3,
        mouseDrag: true,
        margin: 30,
      },
      1200: {
        items: 3,
        mouseDrag: false,
        margin: 30,
      }
    }
  });

  $("[data-fancybox]").fancybox();

});